# Aws Credential
variable "aws_access_key" {
  type        = string
  description = "AWS access key"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS secret key"
}

variable "aws_region" {
  type        = string
  description = "AWS region"
}

# Instance Specification
variable "image_default" {
  type = string
  default = "ami-0b1e534a4ff9019e0"
  description = "Amazon linux 2 AMI"
}

variable "spec_default" {
  type = string
  default = "t3a.medium"
  description = "Specification server default for rhb"
}

variable "region_1a" {
  type = string
  default = "ap-southeast-1a"
  description = "Region 1a for rhb"
}

# Key Pair
variable "default_key" {
  type = string
  default = "sit-rhb"
  description = "Default key pair for server in rhb"
}

# Subnet ID
variable "web_tier" {
  type = string
  default = "subnet-072ac9a8d15fc1624"
  description = "Subnet for server in web tier"
}

variable "app_tier" {
  type = string
  default = "subnet-0317170d5d8575b34"
}

# Security Group ID
variable "zuul_sg" {
  type = list(string)
  default = ["sg-08257935d7079143f"]
  description = "Security group for zuul server"
}

variable "web_sg" {
  type = list(string)
  default = ["sg-02995ecdabe7824d9"]
  description = "Security group for web server"
}

variable "magnolia_public_sg" {
  type = list(string)
  default = ["sg-08257935d7079143f"]
  description = "Security group for magnolia public"
}

variable "magnolia_author_sg" {
  type = list(string)
  default = ["sg-047ae1e3745a343c6"]
  description = "Security group for magnolia author"
}

variable "middleware_sg" {
  type = list(string)
  default = ["sg-0f9ef314aa89325d9"]
  description = "Security group for middleware"
}

variable "app_sg" {
  type = list(string)
  default = ["sg-050c057df68b0b95a"]
  description = "Security group for app"
}

variable "security_sg" {
  type = list(string)
  default = ["sg-08d052e6b9b98b1ff"]
  description = "Security group for security"
}

variable "tr_sg" {
  type = list(string)
  default = ["sg-0bc404b5ec7b4515e"]
  description = "Security group for tr"
}

variable "report_sg" {
  type = list(string)
  default = ["sg-00227bb333c4ef6ee"]
  description = "Security group for report"
}

variable "myinfo_sg" {
  type = list(string)
  default = ["sg-01aca1497b68581c0"]
  description = "Security group for myinfo"
}




