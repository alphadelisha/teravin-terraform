# Instance AMI 2 - sit-zuul-new
resource "aws_instance" "sit-zuul-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.web_tier
  vpc_security_group_ids = var.zuul_sg

  tags = {
    Name = "sit-zuul-new"
  }
}

# Instance AMI 2 - sit-web-new
resource "aws_instance" "sit-web-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.web_tier
  vpc_security_group_ids = var.web_sg

  tags = {
    Name = "sit-web-new"
  }
}

# Instance AMI 2 - sit-magnolia-public-new
resource "aws_instance" "sit-magnolia-public-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.web_tier
  vpc_security_group_ids = var.magnolia_public_sg

  tags = {
    Name = "sit-magnolia-public-new"
  }
}

# Instance AMI 2 - sit-magnolia-author-new
resource "aws_instance" "sit-magnolia-author-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.web_tier
  vpc_security_group_ids = var.magnolia_author_sg

  tags = {
    Name = "sit-magnolia-author-new"
  }
}

# Instance AMI 2 - sit-middleware-new
resource "aws_instance" "sit-middleware-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.app_tier
  vpc_security_group_ids = var.middleware_sg

  tags = {
    Name = "sit-middleware-new"
  }
}


/* # Instance AMI 2 - sit-myinfo-new
resource "aws_instance" "sit-myinfo-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = "subnet-0c2d23b1c8b3c1e63"
  vpc_security_group_ids = var.myinfo_sg

  tags = {
    Name = "sit-myinfo-new"
  }
} */

# Instance AMI 2 - sit-app-new
resource "aws_instance" "sit-app-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.app_tier
  vpc_security_group_ids = var.app_sg

  tags = {
    Name = "sit-app-new"
  }
}

# Instance AMI 2 - sit-security-new
resource "aws_instance" "sit-security-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.app_tier
  vpc_security_group_ids = var.security_sg

  tags = {
    Name = "sit-security-new"
  }
}

# Instance AMI 2 - sit-tr-new
resource "aws_instance" "sit-tr-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.app_tier
  vpc_security_group_ids = var.tr_sg

  tags = {
    Name = "sit-tr-new"
  }
}

# Instance AMI 2 - sit-scsserver-new
resource "aws_instance" "sit-scsserver-author-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.app_tier
  vpc_security_group_ids = var.web_sg

  tags = {
    Name = "sit-scsserver-new"
  }
}

# Instance AMI 2 - sit-report-new
resource "aws_instance" "sit-report-new" {
  ami                    = var.image_default
  instance_type          = var.spec_default
  availability_zone      = var.region_1a
  key_name               = var.default_key
  subnet_id              = var.app_tier
  vpc_security_group_ids = var.report_sg

  tags = {
    Name = "sit-report-new"
  }
}













