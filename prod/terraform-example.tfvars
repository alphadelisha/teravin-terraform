/*

How to use :
    - Rename this file into terraform.tfvars
    - Input your aws_access_key, aws_secret_key, aws_region
    - Save this file
*/

# AWS Settings
aws_access_key = ""
aws_secret_key = ""
aws_region     = ""